/* global S, describe, it, expect, beforeEach, jasmine */

describe("S()", () => {
	describe("creation", () => {
		it("throws if no function passed in", () => {
			S.root(() => {
				expect(() => {
					S();
				}).toThrow();
			});
		});

		it("throws if arg is not a function", () => {
			S.root(() => {
				expect(() => {
					S(1);
				}).toThrow();
			});
		});

		it("generates a function", () => {
			S.root(() => {
				const f = S(() => 1);
				expect(f).toEqual(jasmine.any(Function));
			});
		});

		it("returns initial value of wrapped function", () => {
			S.root(() => {
				const f = S(() => 1);
				expect(f()).toBe(1);
			});
		});
	});

	describe("evaluation", () => {
		it("occurs once intitially", () => {
			S.root(() => {
				const spy = jasmine.createSpy();
				const f = S(spy);
				expect(spy.calls.count()).toBe(1);
			});
		});

		it("does not re-occur when read", () => {
			S.root(() => {
				const spy = jasmine.createSpy();
				const f = S(spy);
				f();
				f();
				f();

				expect(spy.calls.count()).toBe(1);
			});
		});
	});

	describe("with a dependency on an S.data", () => {
		it("updates when S.data is set", () => {
			S.root(() => {
				const d = S.data(1);
				let fevals = 0;
				const f = S(() => {
					fevals++;
					return d();
				});

				fevals = 0;

				d(1);
				expect(fevals).toBe(1);
			});
		});

		it("does not update when S.data is read", () => {
			S.root(() => {
				const d = S.data(1);
				let fevals = 0;
				const f = S(() => {
					fevals++;
					return d();
				});

				fevals = 0;

				d();
				expect(fevals).toBe(0);
			});
		});

		it("updates return value", () => {
			S.root(() => {
				const d = S.data(1);
				let fevals = 0;
				const f = S(() => {
					fevals++;
					return d();
				});

				fevals = 0;

				d(2);
				expect(f()).toBe(2);
			});
		});
	});

	describe("with changing dependencies", () => {
		let i, t, e, fevals, f;

		function init() {
			i = S.data(true);
			t = S.data(1);
			e = S.data(2);
			fevals = 0;
			f = S(() => {
				fevals++;
				return i() ? t() : e();
			});
			fevals = 0;
		}

		it("updates on active dependencies", () => {
			S.root(() => {
				init();
				t(5);
				expect(fevals).toBe(1);
				expect(f()).toBe(5);
			});
		});

		it("does not update on inactive dependencies", () => {
			S.root(() => {
				init();
				e(5);
				expect(fevals).toBe(0);
				expect(f()).toBe(1);
			});
		});

		it("deactivates obsolete dependencies", () => {
			S.root(() => {
				init();
				i(false);
				fevals = 0;
				t(5);
				expect(fevals).toBe(0);
			});
		});

		it("activates new dependencies", () => {
			S.root(() => {
				init();
				i(false);
				fevals = 0;
				e(5);
				expect(fevals).toBe(1);
			});
		});

		it("insures that new dependencies are updated before dependee", () => {
			S.root(() => {
				let order = "";
				const a = S.data(0);
				const b = S(() => {
					order += "b";
					return a() + 1;
				});
				const c = S(() => {
					order += "c";
					return b() || d();
				});
				const d = S(() => {
					order += "d";
					return a() + 10;
				});

				expect(order).toBe("bcd");

				order = "";
				a(-1);

				expect(order).toBe("bcd");
				expect(c()).toBe(9);

				order = "";
				a(0);

				expect(order).toBe("bcd");
				expect(c()).toBe(1);
			});
		});
	});

	describe("that creates an S.data", () => {
		it("does not register a dependency", () => {
			S.root(() => {
				let fevals = 0;
				const f = S(() => {
					fevals++;
					d = S.data(1);
				});
				fevals = 0;
				d(2);
				expect(fevals).toBe(0);
			});
		});
	});

	describe("from a function with no return value", () => {
		it("reads as undefined", () => {
			S.root(() => {
				const f = S(() => {});
				expect(f()).not.toBeDefined();
			});
		});
	});

	describe("with a seed", () => {
		it("reduces seed value", () => {
			S.root(() => {
				const a = S.data(5);
				const f = S((v) => v + a(), 5);
				expect(f()).toBe(10);
				a(6);
				expect(f()).toBe(16);
			});
		});
	});

	describe("with a dependency on a computation", () => {
		let d, fcount, f, gcount, g;

		function init() {
			d = S.data(1);
			fcount = 0;
			f = S(() => {
				fcount++;
				return d();
			});
			gcount = 0;
			g = S(() => {
				gcount++;
				return f();
			});
		}

		it("does not cause re-evaluation", () => {
			S.root(() => {
				init();
				expect(fcount).toBe(1);
			});
		});

		it("does not occur from a read", () => {
			S.root(() => {
				init();
				f();
				expect(gcount).toBe(1);
			});
		});

		it("does not occur from a read of the watcher", () => {
			S.root(() => {
				init();
				g();
				expect(gcount).toBe(1);
			});
		});

		it("occurs when computation updates", () => {
			S.root(() => {
				init();
				d(2);
				expect(fcount).toBe(2);
				expect(gcount).toBe(2);
				expect(g()).toBe(2);
			});
		});
	});

	describe("with unending changes", () => {
		it("throws when continually setting a direct dependency", () => {
			S.root(() => {
				const d = S.data(1);

				expect(() => {
					S(() => {
						d();
						d(2);
					});
				}).toThrow();
			});
		});

		it("throws when continually setting an indirect dependency", () => {
			S.root(() => {
				const d = S.data(1);
				const f1 = S(() => d());
				const f2 = S(() => f1());
				const f3 = S(() => f2());

				expect(() => {
					S(() => {
						f3();
						d(2);
					});
				}).toThrow();
			});
		});
	});

	describe("with circular dependencies", () => {
		it("throws when cycle created by modifying a branch", () => {
			S.root(() => {
				const d = S.data(1);
				// The use of "var" is important here!
				// Because "var" defines a variable before assigning a value,
				// it's the only way to make this self-referencing function work.
				// (let/const break the test)
				var f = S(() => (f ? f() : d()));

				expect(() => d(0)).toThrow();
			});
		});
	});

	describe("with converging dependencies", () => {
		it("propagates in topological order", () => {
			S.root(() => {
				//
				//     c1
				//    /  \
				//   /    \
				//  b1     b2
				//   \    /
				//    \  /
				//     a1
				//
				let seq = "";
				const a1 = S.data(true);
				const b1 = S(() => {
					a1();
					seq += "b1";
				});
				const b2 = S(() => {
					a1();
					seq += "b2";
				});
				const c1 = S(() => {
					b1(), b2();
					seq += "c1";
				});

				seq = "";
				a1(true);

				expect(seq).toBe("b1b2c1");
			});
		});

		it("only propagates once with linear convergences", () => {
			S.root(() => {
				//         d
				//         |
				// +---+---+---+---+
				// v   v   v   v   v
				// f1  f2  f3  f4  f5
				// |   |   |   |   |
				// +---+---+---+---+
				//         v
				//         g
				const d = S.data(0);
				const f1 = S(() => d());
				const f2 = S(() => d());
				const f3 = S(() => d());
				const f4 = S(() => d());
				const f5 = S(() => d());
				let gcount = 0;
				const g = S(() => {
					gcount++;
					return f1() + f2() + f3() + f4() + f5();
				});

				gcount = 0;
				d(0);
				expect(gcount).toBe(1);
			});
		});

		it("only propagates once with exponential convergence", () => {
			S.root(() => {
				//     d
				//     |
				// +---+---+
				// v   v   v
				// f1  f2 f3
				//   \ | /
				//     O
				//   / | \
				// v   v   v
				// g1  g2  g3
				// +---+---+
				//     v
				//     h
				const d = S.data(0);
				const f1 = S(() => d());
				const f2 = S(() => d());
				const f3 = S(() => d());
				const g1 = S(() => f1() + f2() + f3());
				const g2 = S(() => f1() + f2() + f3());
				const g3 = S(() => f1() + f2() + f3());
				let hcount = 0;
				const h = S(() => {
					hcount++;
					return g1() + g2() + g3();
				});

				hcount = 0;
				d(0);
				expect(hcount).toBe(1);
			});
		});
	});
});
