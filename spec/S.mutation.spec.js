describe("Computations which modify data", () => {
	it("freeze data while executing computation", () => {
		S.root(() => {
			const a = S.data(false);
			const b = S.data(0);
			let cb;
			const c = S(() => {
				if (a()) {
					b(1);
					cb = b();
					a(false);
				}
			});

			b(0);
			a(true);

			expect(b()).toBe(1);
			expect(cb).toBe(0);
		});
	});

	it("freeze data while propagating", () => {
		S.root(() => {
			let seq = "";
			const a = S.data(false);
			const b = S.data(0);
			let db;
			const c = S(() => {
				if (a()) {
					seq += "c";
					b(1);
					a(false);
				}
			});
			const d = S(() => {
				if (a()) {
					seq += "d";
					db = b();
				}
			});

			b(0);
			seq = "";
			a(true);

			expect(seq).toBe("cd");
			expect(b()).toBe(1);
			expect(db).toBe(0); // d saw b(0) even though it ran after c whcih modified b() to b(1)
		});
	});

	it("continue running until changes stop", () => {
		S.root(() => {
			let seq = "";
			const a = S.data(0);

			S(() => {
				seq += a();
				if (a() < 10) a(a() + 1);
			});

			expect(seq).toBe("012345678910");
			expect(a()).toBe(10);
		});
	});

	it("propagate changes topologically", () => {
		S.root(() => {
			//
			//    d1      d2
			//    |  \  /  |
			//    |   c1   |
			//    |   ^    |
			//    |   :    |
			//    b1  b2  b3
			//      \ | /
			//        a1
			//
			let seq = "";
			const a1 = S.data(0);
			const c1 = S.data(0);
			const b1 = S(() => a1());
			const b2 = S(() => c1(a1()));
			const b3 = S(() => a1());
			const d1 = S(() => {
				b1();
				seq += "c4(" + c1() + ")";
			});
			const d2 = S(() => {
				b3();
				seq += "c5(" + c1() + ")";
			});

			seq = "";
			a1(1);

			expect(seq).toBe("c4(0)c5(0)c4(1)c5(1)");
		});
	});
});
