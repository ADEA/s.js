describe("S() with subcomputations", () => {
	it("does not register a dependency on the subcomputation", () => {
		S.root(() => {
			const d = S.data(1);
			const spy = jasmine.createSpy("spy");
			const gspy = jasmine.createSpy("gspy");
			const f = S(() => {
				spy();
				const g = S(() => {
					gspy();
					return d();
				});
			});

			spy.calls.reset();
			gspy.calls.reset();

			d(2);

			expect(gspy.calls.count()).toBe(1);
			expect(spy.calls.count()).toBe(0);
		});
	});

	describe("with child", () => {
		let d, e, fspy, f, gspy, g, h;

		function init() {
			d = S.data(1);
			e = S.data(2);
			fspy = jasmine.createSpy("fspy");
			gspy = jasmine.createSpy("gspy");
			f = S(() => {
				fspy();
				d();
				g = S(() => {
					gspy();
					return e();
				});
			});
			h = g;
			h();
		}

		it("creates child on initialization", () => {
			S.root(() => {
				init();
				expect(h).toEqual(jasmine.any(Function));
				expect(h()).toBe(2);
			});
		});

		it("does not depend on child's dependencies", () => {
			S.root(() => {
				init();
				e(3);
				expect(fspy.calls.count()).toBe(1);
				expect(gspy.calls.count()).toBe(2);
			});
		});

		it("disposes old child when updated", () => {
			S.root(() => {
				init();
				// re-evalue parent, thereby disposing stale g, which we've stored at h
				d(2);
				e(3);
				// h is now disposed
				expect(h()).toBe(2);
			});
		});

		it("disposes child when it is disposed", () => {
			const dispose = S.root((dispose) => {
				init();
				return dispose;
			});

			dispose();
			e(3);
			expect(g()).toBe(2);
		});
	});

	describe("which disposes sub that's being updated", () => {
		it("propagates successfully", () => {
			S.root(() => {
				const a = S.data(1);
				const b = S(() => {
					const c = S(() => a());
					a();
					return { c: c };
				});
				const d = S(() => b().c());

				expect(d()).toBe(1);
				a(2);
				expect(d()).toBe(2);
				a(3);
				expect(d()).toBe(3);
			});
		});
	});

	describe("which disposes a sub with a dependee with a sub", () => {
		it("propagates successfully", () => {
			S.root(() => {
				const a = S.data(1);
				let c;
				const b = S(() => {
					c = S(() => a());
					a();
					return { c: c };
				});
				const d = S(() => {
					c();
					const e = S(() => a());
					return { e: e };
				});

				expect(d().e()).toBe(1);
				a(2);
				expect(d().e()).toBe(2);
				a(3);
				expect(d().e()).toBe(3);
			});
		});
	});
});
