/* globals jasmine */
describe("S.on(...)", () => {
	it("registers a dependency", () => {
		S.root(() => {
			const d = S.data(1);
			const spy = jasmine.createSpy();
			const f = S.on(d, () => {
				spy();
			});

			expect(spy.calls.count()).toBe(1);

			d(2);

			expect(spy.calls.count()).toBe(2);
		});
	});

	it("prohibits dynamic dependencies", () => {
		S.root(() => {
			const d = S.data(1);
			const spy = jasmine.createSpy("spy");
			const s = S.on(
				() => {},
				() => {
					spy();
					return d();
				}
			);

			expect(spy.calls.count()).toBe(1);

			d(2);

			expect(spy.calls.count()).toBe(1);
		});
	});

	it("allows multiple dependencies", () => {
		S.root(() => {
			const a = S.data(1);
			const b = S.data(2);
			const c = S.data(3);
			const spy = jasmine.createSpy();
			const f = S.on(
				() => {
					a();
					b();
					c();
				},
				() => {
					spy();
				}
			);

			expect(spy.calls.count()).toBe(1);

			a(4);
			b(5);
			c(6);

			expect(spy.calls.count()).toBe(4);
		});
	});

	it("allows an array of dependencies", () => {
		S.root(() => {
			const a = S.data(1);
			const b = S.data(2);
			const c = S.data(3);
			const spy = jasmine.createSpy();
			const f = S.on([a, b, c], () => {
				spy();
			});

			expect(spy.calls.count()).toBe(1);

			a(4);
			b(5);
			c(6);

			expect(spy.calls.count()).toBe(4);
		});
	});

	it("modifies its accumulator when reducing", () => {
		S.root(() => {
			const a = S.data(1);
			const c = S.on(
				a,
				(sum) => {
					return sum + a();
				},
				0
			);

			expect(c()).toBe(1);

			a(2);

			expect(c()).toBe(3);

			a(3);
			a(4);

			expect(c()).toBe(10);
		});
	});

	it("suppresses initial run when onchanges is true", () => {
		S.root(() => {
			const a = S.data(1);
			const c = S.on(
				a,
				() => {
					return a() * 2;
				},
				0,
				true
			);

			expect(c()).toBe(0);

			a(2);

			expect(c()).toBe(4);
		});
	});
});
