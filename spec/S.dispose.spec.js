describe("S.root(dispose)", () => {
	it("disables updates and sets computation's value to undefined", () => {
		S.root((dispose) => {
			let c = 0;
			const d = S.data(0);
			const f = S(() => {
				c++;
				return d();
			});

			expect(c).toBe(1);
			expect(f()).toBe(0);

			d(1);

			expect(c).toBe(2);
			expect(f()).toBe(1);

			dispose();

			d(2);

			expect(c).toBe(2);
			expect(f()).toBe(1);
		});
	});

	// unconventional uses of dispose -- to insure S doesn't behaves as expected in these cases

	it("works from the body of its own computation", () => {
		S.root((dispose) => {
			let c = 0;
			const d = S.data(0);
			const f = S(() => {
				c++;
				if (d()) dispose();
				d();
			});

			expect(c).toBe(1);

			d(1);

			expect(c).toBe(2);

			d(2);

			expect(c).toBe(2);
		});
	});

	it("works from the body of a subcomputation", () => {
		S.root((dispose) => {
			let c = 0;
			const d = S.data(0);
			const f = S(() => {
				c++;
				d();
				S(() => {
					if (d()) dispose();
				});
			});

			expect(c).toBe(1);

			d(1);

			expect(c).toBe(2);

			d(2);

			expect(c).toBe(2);
		});
	});
});
