describe("S.value", () => {
	it("takes and returns an initial value", () => {
		expect(S.value(1)()).toBe(1);
	});

	it("can be set by passing in a new value", () => {
		const d = S.value(1);
		d(2);
		expect(d()).toBe(2);
	});

	it("returns value being set", () => {
		const d = S.value(1);
		expect(d(2)).toBe(2);
	});

	it("does not propagate if set to equal value", () => {
		S.root(() => {
			const d = S.value(1);
			let e = 0;
			const f = S(() => {
				d();
				return ++e;
			});

			expect(f()).toBe(1);
			d(1);
			expect(f()).toBe(1);
		});
	});

	it("propagate if set to unequal value", () => {
		S.root(() => {
			const d = S.value(1);
			let e = 0;
			const f = S(() => {
				d();
				return ++e;
			});

			expect(f()).toBe(1);
			d(1);
			expect(f()).toBe(1);
			d(2);
			expect(f()).toBe(2);
		});
	});

	it("can take an equality predicate", () => {
		S.root(() => {
			const d = S.value([1], (a, b) => a[0] === b[0]);
			let e = 0;
			const f = S(() => {
				d();
				return ++e;
			});

			expect(f()).toBe(1);
			d([1]);
			expect(f()).toBe(1);
			d([2]);
			expect(f()).toBe(2);
		});
	});
});
