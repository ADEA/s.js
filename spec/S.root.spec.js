/* globals S, describe, it, expect */

describe("S.root()", () => {
	it("allows subcomputations to escape their parents", () => {
		S.root(() => {
			const outerTrigger = S.data(null);
			const innerTrigger = S.data(null);
			let outer;
			let innerRuns = 0;

			outer = S(() => {
				// register dependency to outer trigger
				outerTrigger();
				// inner computation
				S.root(() => {
					S(() => {
						// register dependency on inner trigger
						innerTrigger();
						// count total runs
						innerRuns++;
					});
				});
			});

			// at start, we have one inner computation, that's run once
			expect(innerRuns).toBe(1);

			// trigger the outer computation, making more inners
			outerTrigger(null);
			outerTrigger(null);

			expect(innerRuns).toBe(3);

			// now trigger inner signal: three orphaned computations should equal three runs
			innerRuns = 0;
			innerTrigger(null);

			expect(innerRuns).toBe(3);
		});
	});

	//it("is necessary to create a toplevel computation", function () {
	//    expect(() => {
	//        S(() => 1)
	//    }).toThrowError(/root/);
	//});

	it("does not freeze updates when used at top level", () => {
		S.root(() => {
			const s = S.data(1);
			const c = S(() => s());

			expect(c()).toBe(1);

			s(2);

			expect(c()).toBe(2);

			s(3);

			expect(c()).toBe(3);
		});
	});

	it("persists through entire scope when used at top level", () => {
		S.root(() => {
			const s = S.data(1);
			const c1 = S(() => s());

			s(2);

			const c2 = S(() => s());

			s(3);

			expect(c2()).toBe(3);
		});
	});
});
