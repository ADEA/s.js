/* globals describe, it, expect */
describe("S.sample(...)", () => {
	it("avoids a depdendency", () => {
		S.root(() => {
			const a = S.data(1);
			const b = S.data(2);
			const c = S.data(3);
			let d = 0;
			const e = S(() => {
				d++;
				a();
				S.sample(b);
				c();
			});

			expect(d).toBe(1);

			b(4);

			expect(d).toBe(1);

			a(5);
			c(6);

			expect(d).toBe(3);
		});
	});
});
