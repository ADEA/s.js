describe("S.freeze", () => {
	it("batches changes until end", () => {
		const d = S.data(1);

		S.freeze(() => {
			d(2);
			expect(d()).toBe(1);
		});

		expect(d()).toBe(2);
	});

	it("halts propagation within its scope", () => {
		S.root(() => {
			const d = S.data(1);
			const f = S(() => d());

			S.freeze(() => {
				d(2);
				expect(f()).toBe(1);
			});

			expect(f()).toBe(2);
		});
	});
});
