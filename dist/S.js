(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
    typeof define === 'function' && define.amd ? define(factory) :
    (global = typeof globalThis !== 'undefined' ? globalThis : global || self, global.S = factory());
})(this, (function () { 'use strict';

    class Queue {
        items = [];
        count = 0;
        reset() {
            this.count = 0;
        }
        add(item) {
            this.items[this.count++] = item;
        }
        run(fn) {
            const items = this.items;
            for (let i = 0; i < this.count; i++) {
                fn(items[i]);
                items[i] = null;
            }
            this.count = 0;
        }
    }

    class Clock {
        time = 0;
        changes = new Queue(); // batched changes to data nodes
        updates = new Queue(); // computations to update
        disposes = new Queue(); // disposals to run after current batch of updates finishes
    }

    // "Globals" used to keep track of current system state
    class SGlobals {
        static RootClock = new Clock();
        static RunningClock = null;
        static Listener = null; // currently listening computation
        static Owner = null; // owner for new computations
        static LastNode = null; // cached unused node, for re-use
        static makeComputationNodeResult = {
            node: null,
            value: undefined,
        };
        static RootClockProxy = new Proxy(SGlobals.RootClock, {
            get(target, prop, receiver) {
                if (prop !== "time")
                    return;
                return Reflect.get(target, prop, receiver);
            },
        });
    }

    class Log {
        node1 = null;
        node1slot = 0;
        nodes = null;
        nodeslots = null;
    }

    // Functions
    function isListening() {
        return SGlobals.Listener !== null;
    }
    function isFrozen() {
        return SGlobals.RunningClock !== null;
    }
    function disposeNode(node) {
        if (SGlobals.RunningClock === null) {
            dispose(node);
            return;
        }
        SGlobals.RootClock.disposes.add(node);
    }
    function makeDataNode(value) {
        return new DataNode(value);
    }
    function makeComputationNode(fn, value, orphan, sample) {
        const node = getCandidateNode();
        const owner = SGlobals.Owner;
        const listener = SGlobals.Listener;
        const toplevel = !isFrozen();
        SGlobals.Owner = node;
        SGlobals.Listener = sample ? null : node;
        value = toplevel ? execToplevelComputation(fn, value) : fn(value);
        SGlobals.Owner = owner;
        SGlobals.Listener = listener;
        const recycled = recycleOrClaimNode(node, fn, value, orphan);
        if (toplevel)
            finishToplevelComputation(owner, listener);
        SGlobals.makeComputationNodeResult.node = recycled ? null : node;
        SGlobals.makeComputationNodeResult.value = value;
        return SGlobals.makeComputationNodeResult;
    }
    function execToplevelComputation(fn, value) {
        SGlobals.RunningClock = SGlobals.RootClock;
        SGlobals.RootClock.changes.reset();
        SGlobals.RootClock.updates.reset();
        try {
            return fn(value);
        }
        finally {
            SGlobals.Owner = SGlobals.Listener = SGlobals.RunningClock = null;
        }
    }
    function finishToplevelComputation(owner, listener) {
        if (SGlobals.RootClock.changes.count <= 0 &&
            SGlobals.RootClock.updates.count <= 0)
            return;
        SGlobals.RootClock.time++;
        try {
            run(SGlobals.RootClock);
        }
        finally {
            SGlobals.RunningClock = null;
            SGlobals.Owner = owner;
            SGlobals.Listener = listener;
        }
    }
    function getCandidateNode() {
        const node = SGlobals.LastNode ?? new ComputationNode();
        SGlobals.LastNode = null;
        return node;
    }
    function recycleOrClaimNode(node, fn, value, orphan) {
        const _owner = orphan || SGlobals.Owner === null || SGlobals.Owner === UNOWNED
            ? null
            : SGlobals.Owner;
        const recycle = node.source1 === null &&
            ((node.owned === null && node.cleanups === null) || _owner !== null);
        let i;
        if (!recycle) {
            node.fn = fn;
            node.value = value;
            node.age = SGlobals.RootClock.time;
            if (_owner !== null) {
                _owner.owned ??= [];
                _owner.owned.push(node);
            }
            return recycle;
        }
        SGlobals.LastNode = node;
        const pusher = (watch) => {
            const len = node[watch].length;
            const push = watch === "owned"
                ? (i) => _owner.owned.push(node.owned[i])
                : (i) => _owner.cleanups.push(node.cleanups[i]);
            for (i = 0; i < len; i++) {
                push(i);
            }
        };
        if (_owner !== null) {
            if (node.owned !== null && _owner.owned !== null) {
                pusher("owned");
            }
            if (node.cleanups !== null && _owner.cleanups !== null) {
                pusher("cleanups");
            }
            _owner.owned ??= node.owned;
            _owner.cleanups ??= node.cleanups;
            node.owned = node.cleanups = null;
        }
        return recycle;
    }
    function logRead(from) {
        const to = SGlobals.Listener;
        let fromslot;
        const toslot = to.source1 === null ? -1 : to.sources === null ? 0 : to.sources.length;
        if (from.node1 === null) {
            from.node1 = to;
            from.node1slot = toslot;
            fromslot = -1;
        }
        else if (from.nodes === null) {
            from.nodes = [to];
            from.nodeslots = [toslot];
            fromslot = 0;
        }
        else {
            fromslot = from.nodes.length;
            from.nodes.push(to);
            from.nodeslots.push(toslot);
        }
        if (to.source1 === null) {
            to.source1 = from;
            to.source1slot = fromslot;
        }
        else if (to.sources === null) {
            to.sources = [from];
            to.sourceslots = [fromslot];
        }
        else {
            to.sources.push(from);
            to.sourceslots.push(fromslot);
        }
    }
    function logDataRead(data) {
        logRead((data.log ??= new Log()));
    }
    function logComputationRead(node) {
        logRead((node.log ??= new Log()));
    }
    function event() {
        // b/c we might be under a top level S.root(), have to preserve current root
        const owner = SGlobals.Owner;
        SGlobals.RootClock.updates.reset();
        SGlobals.RootClock.time++;
        try {
            run(SGlobals.RootClock);
        }
        finally {
            SGlobals.RunningClock = SGlobals.Listener = null;
            SGlobals.Owner = owner;
        }
    }
    function run(clock) {
        const running = SGlobals.RunningClock;
        let count = 0;
        SGlobals.RunningClock = clock;
        clock.disposes.reset();
        const hasBatch = () => clock.changes.count !== 0 ||
            clock.updates.count !== 0 ||
            clock.disposes.count !== 0;
        // for each batch ...
        while (hasBatch()) {
            if (count > 0)
                // don't tick on first run, or else we expire already scheduled updates
                clock.time++;
            clock.changes.run(applyDataChange);
            clock.updates.run(updateNode);
            clock.disposes.run(dispose);
            // if there are still changes after excessive batches, assume runaway
            if (count++ > 1e5) {
                throw new Error("Runaway clock detected");
            }
        }
        SGlobals.RunningClock = running;
    }
    function applyDataChange(data) {
        data.value = data.pending;
        data.pending = NOTPENDING;
        if (data.log)
            markComputationsStale(data.log);
    }
    function markComputationsStale(log) {
        const node1 = log.node1;
        const nodes = log.nodes;
        // mark all downstream nodes stale which haven't been already
        if (node1 !== null)
            markNodeStale(node1);
        if (nodes !== null) {
            for (let i = 0, len = nodes.length; i < len; i++) {
                markNodeStale(nodes[i]);
            }
        }
    }
    function markNodeStale(node) {
        const time = SGlobals.RootClock.time;
        if (node.age >= time)
            return;
        node.age = time;
        node.state = STALE;
        SGlobals.RootClock.updates.add(node);
        if (node.owned !== null)
            markOwnedNodesForDisposal(node.owned);
        if (node.log !== null)
            markComputationsStale(node.log);
    }
    function markOwnedNodesForDisposal(owned) {
        const len = owned.length;
        for (let i = 0; i < len; i++) {
            const child = owned[i];
            child.age = SGlobals.RootClock.time;
            child.state = CURRENT;
            if (child.owned !== null)
                markOwnedNodesForDisposal(child.owned);
        }
    }
    function updateNode(node) {
        if (node.state !== STALE)
            return;
        const [owner, listener] = [SGlobals.Owner, SGlobals.Listener];
        SGlobals.Owner = SGlobals.Listener = node;
        node.state = RUNNING;
        cleanup(node, false);
        node.value = node.fn(node.value);
        node.state = CURRENT;
        [SGlobals.Owner, SGlobals.Listener] = [owner, listener];
    }
    function cleanup(node, final) {
        const source1 = node.source1;
        const sources = node.sources;
        const sourceslots = node.sourceslots;
        const cleanups = node.cleanups;
        const owned = node.owned;
        let i;
        let len;
        if (cleanups !== null) {
            for (i = 0, len = cleanups.length; i < len; i++) {
                cleanups[i](final);
            }
        }
        if (owned !== null) {
            for (i = 0, len = owned.length; i < len; i++) {
                dispose(owned[i]);
            }
        }
        if (source1 !== null) {
            cleanupSource(source1, node.source1slot);
        }
        if (sources !== null) {
            for (i = 0, len = sources.length; i < len; i++) {
                cleanupSource(sources.pop(), sourceslots.pop());
            }
        }
        node.cleanups = node.owned = node.source1 = null;
    }
    function cleanupSource(source, slot) {
        const nodes = source.nodes;
        const nodeslots = source.nodeslots;
        let last;
        let lastslot;
        if (slot === -1) {
            source.node1 = null;
            return;
        }
        last = nodes.pop();
        lastslot = nodeslots.pop();
        if (slot === nodes.length)
            return;
        nodes[slot] = last;
        nodeslots[slot] = lastslot;
        if (lastslot === -1) {
            last.source1slot = slot;
        }
        else {
            last.sourceslots[lastslot] = slot;
        }
    }
    function dispose(node) {
        node.fn = node.log = null;
        cleanup(node, true);
    }

    class ComputationNode {
        fn = null;
        value = undefined;
        age = -1;
        state = CURRENT;
        source1 = null;
        source1slot = 0;
        sources = null;
        sourceslots = null;
        log = null;
        owned = null;
        cleanups = null;
        constructor() { }
        current() {
            const hasAge = this.age === SGlobals.RootClock.time;
            const isRunning = this.state === RUNNING;
            if (isListening()) {
                if (hasAge && isRunning)
                    throw new Error("circular dependency");
                else if (hasAge)
                    updateNode(this); // checks for state === STALE internally, so don't need to check here
                logComputationRead(this);
            }
            return this.value;
        }
        clock() {
            return SGlobals.RootClockProxy;
        }
    }

    const NOTPENDING = Symbol("NOTPENDING");
    const CURRENT = Symbol("CURRENT");
    const STALE = Symbol("STALE");
    const RUNNING = Symbol("RUNNING");
    const UNOWNED = new ComputationNode();

    class DataNode {
        value = null;
        pending = NOTPENDING;
        log = null;
        constructor(value) {
            this.value = value;
        }
        current() {
            if (SGlobals.Listener !== null) {
                logDataRead(this);
            }
            return this.value;
        }
        next(value) {
            const shouldBatch = isFrozen(); // check if should batch requests
            const valueWasSet = this.pending !== NOTPENDING; // check if value has been set at least once
            const valueConflict = value !== this.pending; // requires that value is set only once
            const shouldLog = this.log !== null; // respond to change with a logger
            if (shouldBatch && valueWasSet && valueConflict) {
                throw new Error(`conflicting changes: "${value}" !== ${this.pending}`);
            }
            if (shouldBatch && !valueWasSet) {
                this.pending = value;
                SGlobals.RootClock.changes.add(this);
            }
            if (!shouldBatch && shouldLog) {
                this.pending = value;
                SGlobals.RootClock.changes.add(this);
                event();
            }
            if (!shouldBatch && !shouldLog) {
                this.value = value;
            }
            return value;
        }
        clock() {
            return SGlobals.RootClockProxy;
        }
    }

    /**
     * @function
     * @name S
     * @description Global S module. Direct function invocation creates the computation dependency tree.
     * @param {() => {}} fn The computation.
     * @param {T} [value] The first value to use if the computation takes an argument (like Array.prototype.reduce())
     * @example
     * ```js
     * const a = S.data(0),
     *       b = S(() => a());
     * console.assert(b() === 0);
     * a(1);
     * console.assert(b() === 1);
     * ```
     */
    const S = function S(fn, value) {
        if (SGlobals.Owner === null)
            console.warn("computations created without a root or parent will never be disposed");
        const { node, value: _value } = makeComputationNode(fn, value, false, false);
        const isNull = node === null;
        const returnValue = () => _value;
        const returnCurrentNode = () => node.current();
        const computationReturn = isNull ? returnValue : returnCurrentNode;
        return function computation() {
            return computationReturn();
        };
    };
    // compatibility with commonjs systems that expect default export to be at require('s.js').default rather than just require('s-js')
    Object.defineProperty(S, "default", { value: S });
    /**
     * @function
     * @name S.root
     * @description Creates a parent for inner computations before disposing them.
     * Avoids leaking computations to top-level without cleaning unused results.
     * All computations should be wrapped in "S.root()" unless you really need top-level.
     * @memberof S
     * @param fn Parent function to execute in the scope, like in e.g. "setInterval".
     *
     * @example
     * ```js
     * S.root(() => {
     *   const a = S.data(0);
     *   const f = S(() => a());
     *   a(1);
     * })
     * ```
     */
    S.root = function root(fn) {
        const noArguments = fn.length === 0;
        const disposerFunction = function _dispose() {
            if (root === null)
                return;
            if (S.isFrozen()) {
                SGlobals.RootClock.disposes.add(root);
                return;
            }
            dispose(root);
        };
        const owner = SGlobals.Owner;
        const disposer = noArguments ? null : disposerFunction;
        let root = disposer === null ? UNOWNED : getCandidateNode();
        let result;
        SGlobals.Owner = root;
        try {
            result = disposer === null ? fn() : fn(disposer);
        }
        finally {
            SGlobals.Owner = owner;
        }
        if (disposer !== null &&
            recycleOrClaimNode(root, null, undefined, true)) {
            root = null;
        }
        return result;
    };
    /**
     * @function
     * @name S.on
     * @description Wrapper around S() to excplicitely tell which nodes are dependencies.
     * @param {() => any | [() => any]} ev Function or array of functions to consider as dependencies.
     * @param {([T]) => T} fn Computation.
     * @param {T} [seed] Initial value if the computation takes an argument, like Array.prototype.reduce.
     * @param {boolean} [onchanges] If set to true, the computation won't run on definition bun only after at least one change has occured.
     * @returns {T | undefined} The return value of the computation.
     *
     * @example
     * ```js
     * const [a, b, c] = [S.data(0), S.data(0), S.data(0)];
     * const f = S.on([a, c], (count) => {
     *   a(); b(); c();
     *   return count++;
     * }, 1, true);
     * a(); // Will increment the count
     * b(); // Will not increment the count
     * c(); // Will increment the count
     * ```
     */
    S.on = function on(ev, fn, seed, onchanges) {
        if (Array.isArray(ev))
            ev = callAll(ev);
        onchanges = !!onchanges;
        return S(on, seed);
        function on(value) {
            const listener = SGlobals.Listener;
            ev();
            if (onchanges)
                onchanges = false;
            else {
                SGlobals.Listener = null;
                value = fn(value);
                SGlobals.Listener = listener;
            }
            return value;
        }
    };
    function callAll(ss) {
        return function all() {
            for (let i = 0; i < ss.length; i++)
                ss[i]();
        };
    }
    /**
     * @function
     * @name S.effect
     * @memberof S
     * @description Alias for S.makeComputationNode where "orphan" and "sample" are both set to false.
     * @param {([T]) => T} fn The computation.
     * @param {T} [value] The initial value of the node.
     * @example
     * ```js
     * const { node, value } = S.effect((v) => v + 1, 0);
     * console.assert(value === 0);
     * console.assert(node.current() === 0);
     * console.assert(node.next(1) === 1);
     * console.assert(node.current() === 1);
     * ```
     */
    S.effect = function effect(fn, value) {
        makeComputationNode(fn, value, false, false);
    };
    /**
     * @function
     * @name S.data
     * @memberof S
     * @description Creates a node to watch.
     * @param {T} value The initial value.
     * @returns {([T]) => T} The function that acts as a proxy to get/set the node's value.
     *
     * @example
     * ```js
     * const a = S.data(0);
     * console.assert(a() === 0);
     * console.assert(a(1) === 1);
     * console.assert(a() === 1);
     * ```
     */
    S.data = function data(value) {
        const node = new DataNode(value);
        return function data(value) {
            const noArguments = arguments.length === 0;
            return noArguments ? node.current() : node.next(value);
        };
    };
    /**
     * @function
     * @name S.value
     * @memberof S
     * @description Creates a node that will emit updates only when it's value becomes different (i.e., not when it is set to the current value).
     * @param {T} current The initial value of the node.
     * @param {(T,T) => boolean} [eq] A comparison function to check for equality
     * @returns {*} The function that acts as a proxy to get/set the node's value.
     *
     * @example
     * ```js
     * const a = S.value({id: 1, name: "adam"}, (x,y) => x.id === y.id);
     * const f = S.on(a, () => console.log(a()));
     * a({id: 1, name: "adam"}); // Will not fire the log.
     * a({id: 1, name: "other"}); // Will not fire the log.
     * a({id: 2, name: "other"}); // Will log the object.
     * ```
     */
    S.value = function value(current, eq) {
        const node = new DataNode(current);
        let age = -1;
        return function value(update) {
            if (arguments.length === 0) {
                return node.current();
            }
            const same = eq ? eq(current, update) : current === update;
            if (!same) {
                let time = SGlobals.RootClock.time;
                if (age === time)
                    throw new Error(`conflicting values: "${update}" is not the same as ${current}`);
                [age, current] = [time, update];
                node.next(update);
            }
            return update;
        };
    };
    /**
     * @function
     * @name S.freeze
     * @memberof S
     * @description Freeze the internal clock to apply multiple updates.
     * @param {() => T} fn The parent function of the computations.
     * @returns {T} The returned result of the computation.
     * @example
     * ```js
     * const [a, b] = [S.data(0), S.data(1)];
     * const f = S.freeze(() => {
     *   a(b()); b(b() + 1); // These changes will run together.
     *   a(); b(); // Still returns 0 and 1, changes haven't been applied yet.
     * });
     * console.assert(a() === 1 && b() === 2); // Now the changes have taken place.
     * ```
     */
    S.freeze = function freeze(fn) {
        if (S.isFrozen()) {
            return fn();
        }
        let result = undefined;
        SGlobals.RunningClock = SGlobals.RootClock;
        SGlobals.RunningClock.changes.reset();
        try {
            result = fn();
            event();
        }
        finally {
            SGlobals.RunningClock = null;
        }
        return result;
    };
    /**
     * @function
     * @name S.sample
     * @memberof S
     * @description Fetches a node's value without firing any event (usefull for nested computations).
     * @param {() => T} fn The data node to fetch.
     * @returns {T} The node's value.
     * @example
     * ```js
     * const a = S.data(0);
     * const f = S(() => a(S.sample(a) + 1));
     * // "a" is acted uppon in "f",
     * // but not a dependency since we don't read it, only assign a value.
     * // "S(() => a(a() + 1))" would have caused an error because "a" is a dependency,
     * // so the computation is rerun infinitely
     * ```
     */
    S.sample = function sample(fn) {
        let result;
        const listener = SGlobals.Listener;
        SGlobals.Listener = null;
        result = fn();
        SGlobals.Listener = listener;
        return result;
    };
    /**
     * @function
     * @name S.cleanup
     * @memberof S
     * @description Run a given cleanup function before updating/disposing a computation.
     * @param {(boolean) => void} fn The cleanup function.
     * @example
     * ```js
     * const a = S.value(true);
     * const f = S(() => {
     *   if (a()) {
     *     const onClickHandler = e => doSomething();
     *     el.addEventListener('click', onClickHandler);
     *     S.cleanup(() => el.removeEventListener('click', onClickHandler));
     *     // Now, the event handler will be removed everytime the value of 'a' changes,
     *     // otherwise you would get multiple listeners that would never be removed.
     *   }
     * })
     * ```
     */
    S.cleanup = function cleanup(fn) {
        if (SGlobals.Owner === null)
            console.warn("cleanups created without a root or parent will never be run");
        else if (SGlobals.Owner.cleanups === null)
            SGlobals.Owner.cleanups = [fn];
        else
            SGlobals.Owner.cleanups.push(fn);
    };
    // experimental : exposing node constructors and some state
    S.makeDataNode = makeDataNode;
    /**
     * @function
     * @description Creates a node from a computation.
     * @param {([T]) => T} fn The computation.
     * @param {T} value The initial value of the node.
     * @param {boolean} orphan Is the computation top-level.
     * @param {boolean} sample Is the value taken by a sample (disable event listening/firing) or normally.
     * @example
     * ```js
     * const { node, value } = S.makeComputationNode((v) => v + 1, 0, false, false);
     * console.assert(value === 0);
     * console.assert(node.current() === 0);
     * console.assert(node.next(1) === 1);
     * console.assert(node.current() === 1);
     * ```
     */
    S.makeComputationNode = makeComputationNode;
    /**
     * @function
     * @name S.disposeNode
     * @memberof S
     * @description Disposes of the node, cleanup function.
     * @param {ComputationNode} node The node to clean.
     * @example
     * ```js
     * const { node, value } = S.effect((v) => v + 1, 0);
     * S.disposeNode(node);
     * ```
     */
    S.disposeNode = disposeNode;
    /**
     * @function
     * @name S.isFrozen
     * @memberof S
     * @description Checks if the internal clock of S is frozen (i.e. called from S.freeze()).
     * @returns {boolean} True if frozen.
     * @example
     * ```js
     * // From top-level...
     * console.assert(S.isFrozen() === false);
     * S.freeze(() => {
     *   console.assert(S.isFrozen() === true);
     * });
     * console.assert(S.isFrozen() === false);
     * ```
     */
    S.isFrozen = isFrozen;
    /**
     * @function
     * @name S.isListening
     * @memberof S
     * @description Checks if the listener is on (disabled in S.sample()).
     * @returns {boolean} True if listening.
     * @example
     * ```js
     * console.assert(S.isListening() === true)
     * ```
     */
    S.isListening = isListening;

    return S;

}));
