import { Queue } from "./queue";
export class Clock {
    time = 0;
    changes = new Queue(); // batched changes to data nodes
    updates = new Queue(); // computations to update
    disposes = new Queue(); // disposals to run after current batch of updates finishes
}
