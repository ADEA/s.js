import { SGlobals } from "./globs";
import { ComputationNode } from "./computationNode";
import { DataNode } from "./dataNode";
import { Log } from "./log";
import { UNOWNED, NOTPENDING, STALE, CURRENT, RUNNING } from "./constants";
// Functions
export function isListening() {
    return SGlobals.Listener !== null;
}
export function isFrozen() {
    return SGlobals.RunningClock !== null;
}
export function disposeNode(node) {
    if (SGlobals.RunningClock === null) {
        dispose(node);
        return;
    }
    SGlobals.RootClock.disposes.add(node);
}
export function makeDataNode(value) {
    return new DataNode(value);
}
export function makeComputationNode(fn, value, orphan, sample) {
    const node = getCandidateNode();
    const owner = SGlobals.Owner;
    const listener = SGlobals.Listener;
    const toplevel = !isFrozen();
    SGlobals.Owner = node;
    SGlobals.Listener = sample ? null : node;
    value = toplevel ? execToplevelComputation(fn, value) : fn(value);
    SGlobals.Owner = owner;
    SGlobals.Listener = listener;
    const recycled = recycleOrClaimNode(node, fn, value, orphan);
    if (toplevel)
        finishToplevelComputation(owner, listener);
    SGlobals.makeComputationNodeResult.node = recycled ? null : node;
    SGlobals.makeComputationNodeResult.value = value;
    return SGlobals.makeComputationNodeResult;
}
function execToplevelComputation(fn, value) {
    SGlobals.RunningClock = SGlobals.RootClock;
    SGlobals.RootClock.changes.reset();
    SGlobals.RootClock.updates.reset();
    try {
        return fn(value);
    }
    finally {
        SGlobals.Owner = SGlobals.Listener = SGlobals.RunningClock = null;
    }
}
function finishToplevelComputation(owner, listener) {
    if (SGlobals.RootClock.changes.count <= 0 &&
        SGlobals.RootClock.updates.count <= 0)
        return;
    SGlobals.RootClock.time++;
    try {
        run(SGlobals.RootClock);
    }
    finally {
        SGlobals.RunningClock = null;
        SGlobals.Owner = owner;
        SGlobals.Listener = listener;
    }
}
export function getCandidateNode() {
    const node = SGlobals.LastNode ?? new ComputationNode();
    SGlobals.LastNode = null;
    return node;
}
export function recycleOrClaimNode(node, fn, value, orphan) {
    const _owner = orphan || SGlobals.Owner === null || SGlobals.Owner === UNOWNED
        ? null
        : SGlobals.Owner;
    const recycle = node.source1 === null &&
        ((node.owned === null && node.cleanups === null) || _owner !== null);
    let i;
    if (!recycle) {
        node.fn = fn;
        node.value = value;
        node.age = SGlobals.RootClock.time;
        if (_owner !== null) {
            _owner.owned ??= [];
            _owner.owned.push(node);
        }
        return recycle;
    }
    SGlobals.LastNode = node;
    const pusher = (watch) => {
        const len = node[watch].length;
        const push = watch === "owned"
            ? (i) => _owner.owned.push(node.owned[i])
            : (i) => _owner.cleanups.push(node.cleanups[i]);
        for (i = 0; i < len; i++) {
            push(i);
        }
    };
    if (_owner !== null) {
        if (node.owned !== null && _owner.owned !== null) {
            pusher("owned");
        }
        if (node.cleanups !== null && _owner.cleanups !== null) {
            pusher("cleanups");
        }
        _owner.owned ??= node.owned;
        _owner.cleanups ??= node.cleanups;
        node.owned = node.cleanups = null;
    }
    return recycle;
}
function logRead(from) {
    const to = SGlobals.Listener;
    let fromslot;
    const toslot = to.source1 === null ? -1 : to.sources === null ? 0 : to.sources.length;
    if (from.node1 === null) {
        from.node1 = to;
        from.node1slot = toslot;
        fromslot = -1;
    }
    else if (from.nodes === null) {
        from.nodes = [to];
        from.nodeslots = [toslot];
        fromslot = 0;
    }
    else {
        fromslot = from.nodes.length;
        from.nodes.push(to);
        from.nodeslots.push(toslot);
    }
    if (to.source1 === null) {
        to.source1 = from;
        to.source1slot = fromslot;
    }
    else if (to.sources === null) {
        to.sources = [from];
        to.sourceslots = [fromslot];
    }
    else {
        to.sources.push(from);
        to.sourceslots.push(fromslot);
    }
}
export function logDataRead(data) {
    logRead((data.log ??= new Log()));
}
export function logComputationRead(node) {
    logRead((node.log ??= new Log()));
}
export function event() {
    // b/c we might be under a top level S.root(), have to preserve current root
    const owner = SGlobals.Owner;
    SGlobals.RootClock.updates.reset();
    SGlobals.RootClock.time++;
    try {
        run(SGlobals.RootClock);
    }
    finally {
        SGlobals.RunningClock = SGlobals.Listener = null;
        SGlobals.Owner = owner;
    }
}
function run(clock) {
    const running = SGlobals.RunningClock;
    let count = 0;
    SGlobals.RunningClock = clock;
    clock.disposes.reset();
    const hasBatch = () => clock.changes.count !== 0 ||
        clock.updates.count !== 0 ||
        clock.disposes.count !== 0;
    // for each batch ...
    while (hasBatch()) {
        if (count > 0)
            // don't tick on first run, or else we expire already scheduled updates
            clock.time++;
        clock.changes.run(applyDataChange);
        clock.updates.run(updateNode);
        clock.disposes.run(dispose);
        // if there are still changes after excessive batches, assume runaway
        if (count++ > 1e5) {
            throw new Error("Runaway clock detected");
        }
    }
    SGlobals.RunningClock = running;
}
function applyDataChange(data) {
    data.value = data.pending;
    data.pending = NOTPENDING;
    if (data.log)
        markComputationsStale(data.log);
}
function markComputationsStale(log) {
    const node1 = log.node1;
    const nodes = log.nodes;
    // mark all downstream nodes stale which haven't been already
    if (node1 !== null)
        markNodeStale(node1);
    if (nodes !== null) {
        for (let i = 0, len = nodes.length; i < len; i++) {
            markNodeStale(nodes[i]);
        }
    }
}
function markNodeStale(node) {
    const time = SGlobals.RootClock.time;
    if (node.age >= time)
        return;
    node.age = time;
    node.state = STALE;
    SGlobals.RootClock.updates.add(node);
    if (node.owned !== null)
        markOwnedNodesForDisposal(node.owned);
    if (node.log !== null)
        markComputationsStale(node.log);
}
function markOwnedNodesForDisposal(owned) {
    const len = owned.length;
    for (let i = 0; i < len; i++) {
        const child = owned[i];
        child.age = SGlobals.RootClock.time;
        child.state = CURRENT;
        if (child.owned !== null)
            markOwnedNodesForDisposal(child.owned);
    }
}
export function updateNode(node) {
    if (node.state !== STALE)
        return;
    const [owner, listener] = [SGlobals.Owner, SGlobals.Listener];
    SGlobals.Owner = SGlobals.Listener = node;
    node.state = RUNNING;
    cleanup(node, false);
    node.value = node.fn(node.value);
    node.state = CURRENT;
    [SGlobals.Owner, SGlobals.Listener] = [owner, listener];
}
function cleanup(node, final) {
    const source1 = node.source1;
    const sources = node.sources;
    const sourceslots = node.sourceslots;
    const cleanups = node.cleanups;
    const owned = node.owned;
    let i;
    let len;
    if (cleanups !== null) {
        for (i = 0, len = cleanups.length; i < len; i++) {
            cleanups[i](final);
        }
    }
    if (owned !== null) {
        for (i = 0, len = owned.length; i < len; i++) {
            dispose(owned[i]);
        }
    }
    if (source1 !== null) {
        cleanupSource(source1, node.source1slot);
    }
    if (sources !== null) {
        for (i = 0, len = sources.length; i < len; i++) {
            cleanupSource(sources.pop(), sourceslots.pop());
        }
    }
    node.cleanups = node.owned = node.source1 = null;
}
function cleanupSource(source, slot) {
    const nodes = source.nodes;
    const nodeslots = source.nodeslots;
    let last;
    let lastslot;
    if (slot === -1) {
        source.node1 = null;
        return;
    }
    last = nodes.pop();
    lastslot = nodeslots.pop();
    if (slot === nodes.length)
        return;
    nodes[slot] = last;
    nodeslots[slot] = lastslot;
    if (lastslot === -1) {
        last.source1slot = slot;
    }
    else {
        last.sourceslots[lastslot] = slot;
    }
}
export function dispose(node) {
    node.fn = node.log = null;
    cleanup(node, true);
}
