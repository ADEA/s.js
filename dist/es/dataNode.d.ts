import { IClock } from "./clock";
import { INode } from "./inode";
import { Log } from "./log";
export interface IDataNode<T> extends INode<T> {
    next(value: T): T;
}
export declare class DataNode implements IDataNode<any> {
    value: any;
    pending: any;
    log: Log | null;
    constructor(value: any);
    current(): any;
    next(value: any): any;
    clock(): IClock;
}
