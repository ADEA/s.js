import { ComputationNode } from "./computationNode";
export declare const NOTPENDING: unique symbol;
export declare const CURRENT: unique symbol;
export declare const STALE: unique symbol;
export declare const RUNNING: unique symbol;
export declare const UNOWNED: ComputationNode;
