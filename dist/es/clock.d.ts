import { Queue, QueueInterface } from "./queue";
import { DataNode } from "./dataNode";
import { ComputationNode } from "./computationNode";
export interface IClock {
    time(): number;
}
export interface ClockInterface {
    time: number;
    changes: QueueInterface<DataNode>;
    updates: QueueInterface<ComputationNode>;
    disposes: QueueInterface<ComputationNode>;
}
export declare class Clock implements ClockInterface {
    time: number;
    changes: Queue<DataNode>;
    updates: Queue<ComputationNode>;
    disposes: Queue<ComputationNode>;
}
