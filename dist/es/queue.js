export class Queue {
    items = [];
    count = 0;
    reset() {
        this.count = 0;
    }
    add(item) {
        this.items[this.count++] = item;
    }
    run(fn) {
        const items = this.items;
        for (let i = 0; i < this.count; i++) {
            fn(items[i]);
            items[i] = null;
        }
        this.count = 0;
    }
}
