import { ComputationNode } from "./computationNode";
import { Clock } from "./clock";
export declare class SGlobals {
    static RootClock: Clock;
    static RunningClock: Clock | null;
    static Listener: ComputationNode | null;
    static Owner: ComputationNode | null;
    static LastNode: ComputationNode | null;
    static makeComputationNodeResult: {
        node: ComputationNode | null;
        value: any;
    };
    static RootClockProxy: Clock;
}
