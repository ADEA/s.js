import { Clock } from "./clock";
// "Globals" used to keep track of current system state
export class SGlobals {
    static RootClock = new Clock();
    static RunningClock = null;
    static Listener = null; // currently listening computation
    static Owner = null; // owner for new computations
    static LastNode = null; // cached unused node, for re-use
    static makeComputationNodeResult = {
        node: null,
        value: undefined,
    };
    static RootClockProxy = new Proxy(SGlobals.RootClock, {
        get(target, prop, receiver) {
            if (prop !== "time")
                return;
            return Reflect.get(target, prop, receiver);
        },
    });
}
