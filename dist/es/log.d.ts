import { ComputationNode } from "./computationNode";
export declare class Log {
    node1: ComputationNode | null;
    node1slot: number;
    nodes: ComputationNode[] | null;
    nodeslots: number[] | null;
}
