import { NOTPENDING } from "./constants";
import { logDataRead, event, isFrozen } from "./helperFunctions";
import { SGlobals } from "./globs";
export class DataNode {
    value = null;
    pending = NOTPENDING;
    log = null;
    constructor(value) {
        this.value = value;
    }
    current() {
        if (SGlobals.Listener !== null) {
            logDataRead(this);
        }
        return this.value;
    }
    next(value) {
        const shouldBatch = isFrozen(); // check if should batch requests
        const valueWasSet = this.pending !== NOTPENDING; // check if value has been set at least once
        const valueConflict = value !== this.pending; // requires that value is set only once
        const shouldLog = this.log !== null; // respond to change with a logger
        if (shouldBatch && valueWasSet && valueConflict) {
            throw new Error(`conflicting changes: "${value}" !== ${this.pending}`);
        }
        if (shouldBatch && !valueWasSet) {
            this.pending = value;
            SGlobals.RootClock.changes.add(this);
        }
        if (!shouldBatch && shouldLog) {
            this.pending = value;
            SGlobals.RootClock.changes.add(this);
            event();
        }
        if (!shouldBatch && !shouldLog) {
            this.value = value;
        }
        return value;
    }
    clock() {
        return SGlobals.RootClockProxy;
    }
}
