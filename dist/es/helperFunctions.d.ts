import { ComputationNode } from "./computationNode";
import { DataNode } from "./dataNode";
export declare function isListening(): boolean;
export declare function isFrozen(): boolean;
export declare function disposeNode(node: ComputationNode): void;
export declare function makeDataNode(value: any): DataNode;
export declare function makeComputationNode<T>(fn: (v: T | undefined) => T, value: T | undefined, orphan: boolean, sample: boolean): {
    node: ComputationNode | null;
    value: T;
};
export declare function getCandidateNode(): ComputationNode;
export declare function recycleOrClaimNode<T>(node: ComputationNode, fn: (v: T | undefined) => T, value: T, orphan: boolean): boolean;
export declare function logDataRead(data: DataNode): void;
export declare function logComputationRead(node: ComputationNode): void;
export declare function event(): void;
export declare function updateNode(node: ComputationNode): void;
export declare function dispose(node: ComputationNode): void;
