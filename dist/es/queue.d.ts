export interface QueueInterface<T> {
    items: T[];
    count: number;
    reset: () => void;
    add: (item: T) => void;
    run: (fn: (item: T) => void) => void;
}
export declare class Queue<T> implements QueueInterface<T> {
    items: T[];
    count: number;
    reset(): void;
    add(item: T): void;
    run(fn: (item: T) => void): void;
}
