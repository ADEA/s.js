import { CURRENT, RUNNING } from "./constants";
import { updateNode, logComputationRead, isListening } from "./helperFunctions";
import { SGlobals } from "./globs";
export class ComputationNode {
    fn = null;
    value = undefined;
    age = -1;
    state = CURRENT;
    source1 = null;
    source1slot = 0;
    sources = null;
    sourceslots = null;
    log = null;
    owned = null;
    cleanups = null;
    constructor() { }
    current() {
        const hasAge = this.age === SGlobals.RootClock.time;
        const isRunning = this.state === RUNNING;
        if (isListening()) {
            if (hasAge && isRunning)
                throw new Error("circular dependency");
            else if (hasAge)
                updateNode(this); // checks for state === STALE internally, so don't need to check here
            logComputationRead(this);
        }
        return this.value;
    }
    clock() {
        return SGlobals.RootClockProxy;
    }
}
