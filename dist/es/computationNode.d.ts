import { IClock } from "./clock";
import { INode } from "./inode";
import { Log } from "./log";
export declare class ComputationNode implements INode<any> {
    fn: ((v: any) => any) | null;
    value: any;
    age: number;
    state: symbol;
    source1: Log | null;
    source1slot: number;
    sources: Log[] | null;
    sourceslots: number[] | null;
    log: Log | null;
    owned: ComputationNode[] | null;
    cleanups: ((final: boolean) => void)[] | null;
    constructor();
    current(): any;
    clock(): IClock;
}
