import { IDataNode } from "./dataNode";
import { INode } from "./inode";
import { DataSignal } from "./dataSignal";
export interface S {
    root<T>(fn: (dispose?: () => void) => T): T;
    <T>(fn: () => T): () => T;
    <T>(fn: (v: T) => T, seed: T): () => T;
    on<T>(ev: () => any, fn: () => T): () => T;
    on<T>(ev: () => any, fn: (v: T) => T, seed: T, onchanges?: boolean): () => T;
    effect<T>(fn: () => T): void;
    effect<T>(fn: (v: T) => T, seed: T): void;
    data<T>(value: T): DataSignal<T>;
    value<T>(value: T, eq?: (a: T, b: T) => boolean): DataSignal<T>;
    freeze<T>(fn: () => T): T;
    sample<T>(fn: () => T): T;
    cleanup(fn: (final: boolean) => any): void;
    isFrozen(): boolean;
    isListening(): boolean;
    makeDataNode<T>(value: T): IDataNode<T>;
    makeComputationNode<T, S>(fn: (val: S) => T, seed: S, orphan: boolean, sample: true): {
        node: INode<T> | null;
        value: T;
    };
    makeComputationNode<T, S>(fn: (val: T | S) => T, seed: S, orphan: boolean, sample: boolean): {
        node: INode<T> | null;
        value: T;
    };
    disposeNode(node: INode<any>): void;
}
/**
 * @function
 * @name S
 * @description Global S module. Direct function invocation creates the computation dependency tree.
 * @param {() => {}} fn The computation.
 * @param {T} [value] The first value to use if the computation takes an argument (like Array.prototype.reduce())
 * @example
 * ```js
 * const a = S.data(0),
 *       b = S(() => a());
 * console.assert(b() === 0);
 * a(1);
 * console.assert(b() === 1);
 * ```
 */
declare const S: S;
export default S;
