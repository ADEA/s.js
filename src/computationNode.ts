import { IClock } from "./clock";
import { INode } from "./inode";
import { CURRENT, RUNNING } from "./constants";
import { updateNode, logComputationRead, isListening } from "./helperFunctions";
import { Log } from "./log";
import { SGlobals } from "./globs";

export class ComputationNode implements INode<any> {
	fn = null as ((v: any) => any) | null;
	value = undefined as any;
	age = -1;
	state = CURRENT;
	source1 = null as null | Log;
	source1slot = 0;
	sources = null as null | Log[];
	sourceslots = null as null | number[];
	log = null as Log | null;
	owned = null as ComputationNode[] | null;
	cleanups = null as ((final: boolean) => void)[] | null;

	constructor() {}

	current() {
		const hasAge = this.age === SGlobals.RootClock.time;
		const isRunning = this.state === RUNNING;

		if (isListening()) {
			if (hasAge && isRunning) throw new Error("circular dependency");
			else if (hasAge) updateNode(this); // checks for state === STALE internally, so don't need to check here
			logComputationRead(this);
		}

		return this.value;
	}

	clock() {
		return SGlobals.RootClockProxy as unknown as IClock;
	}
}
