import { Queue, QueueInterface } from "./queue";
import { DataNode } from "./dataNode";
import { ComputationNode } from "./computationNode";

export interface IClock {
	time(): number;
}

export interface ClockInterface {
	time: number;
	changes: QueueInterface<DataNode>;
	updates: QueueInterface<ComputationNode>;
	disposes: QueueInterface<ComputationNode>;
}

export class Clock implements ClockInterface {
	time = 0;

	changes = new Queue<DataNode>(); // batched changes to data nodes
	updates = new Queue<ComputationNode>(); // computations to update
	disposes = new Queue<ComputationNode>(); // disposals to run after current batch of updates finishes
}
