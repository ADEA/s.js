import { ComputationNode } from "./computationNode";

export const NOTPENDING = Symbol("NOTPENDING");
export const CURRENT = Symbol("CURRENT");
export const STALE = Symbol("STALE");
export const RUNNING = Symbol("RUNNING");
export const UNOWNED = new ComputationNode();
