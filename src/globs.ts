import { ComputationNode } from "./computationNode";
import { Clock } from "./clock";

// "Globals" used to keep track of current system state
export class SGlobals {
	static RootClock = new Clock();
	static RunningClock = null as Clock | null;
	static Listener = null as ComputationNode | null; // currently listening computation
	static Owner = null as ComputationNode | null; // owner for new computations
	static LastNode = null as ComputationNode | null; // cached unused node, for re-use
	static makeComputationNodeResult = {
		node: null as null | ComputationNode,
		value: undefined as any,
	};
	static RootClockProxy = new Proxy(SGlobals.RootClock, {
		get(target, prop, receiver) {
			if (prop !== "time") return;
			return Reflect.get(target, prop, receiver);
		},
	});
}
