import { ComputationNode } from "./computationNode";

export class Log {
	node1 = null as null | ComputationNode;
	node1slot = 0;
	nodes = null as null | ComputationNode[];
	nodeslots = null as null | number[];
}
