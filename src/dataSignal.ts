export interface DataSignal<T> {
	(): T;
	(val: T): T;
}
