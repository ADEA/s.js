export interface QueueInterface<T> {
	items: T[];
	count: number;
	reset: () => void;
	add: (item: T) => void;
	run: (fn: (item: T) => void) => void;
}
export class Queue<T> implements QueueInterface<T> {
	items: T[] = [];
	count = 0;

	reset() {
		this.count = 0;
	}

	add(item: T) {
		this.items[this.count++] = item;
	}

	run(fn: (item: T) => void) {
		const items = this.items;
		for (let i = 0; i < this.count; i++) {
			fn(items[i]!);
			items[i] = null!;
		}
		this.count = 0;
	}
}
