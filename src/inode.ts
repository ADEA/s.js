import { IClock } from "./clock";

export interface INode<T> {
	clock(): IClock;
	current(): T;
}
