import { IClock } from "./clock";
import { INode } from "./inode";
import { NOTPENDING } from "./constants";
import { logDataRead, event, isFrozen } from "./helperFunctions";
import { Log } from "./log";
import { SGlobals } from "./globs";

export interface IDataNode<T> extends INode<T> {
	next(value: T): T;
}
export class DataNode implements IDataNode<any> {
	value: any = null;
	pending = NOTPENDING as any;
	log = null as Log | null;

	constructor(value: any) {
		this.value = value;
	}

	current() {
		if (SGlobals.Listener !== null) {
			logDataRead(this);
		}
		return this.value;
	}

	next(value: any) {
		const shouldBatch = isFrozen(); // check if should batch requests
		const valueWasSet = this.pending !== NOTPENDING; // check if value has been set at least once
		const valueConflict = value !== this.pending; // requires that value is set only once
		const shouldLog = this.log !== null; // respond to change with a logger

		if (shouldBatch && valueWasSet && valueConflict) {
			throw new Error(`conflicting changes: "${value}" !== ${this.pending}`);
		}
		if (shouldBatch && !valueWasSet) {
			this.pending = value;
			SGlobals.RootClock.changes.add(this);
		}
		if (!shouldBatch && shouldLog) {
			this.pending = value;
			SGlobals.RootClock.changes.add(this);
			event();
		}
		if (!shouldBatch && !shouldLog) {
			this.value = value;
		}

		return value!;
	}

	clock() {
		return SGlobals.RootClockProxy as unknown as IClock;
	}
}
