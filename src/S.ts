import { ComputationNode } from "./computationNode";
import { DataNode, IDataNode } from "./dataNode";
import { INode } from "./inode";
import {
	makeComputationNode,
	dispose,
	getCandidateNode,
	recycleOrClaimNode,
	event,
	makeDataNode,
	disposeNode,
	isFrozen,
	isListening,
} from "./helperFunctions";
import { SGlobals } from "./globs";
import { UNOWNED } from "./constants";
import { DataSignal } from "./dataSignal";

export interface S {
	// Computation root
	root<T>(fn: (dispose?: () => void) => T): T;

	// Computation constructors
	<T>(fn: () => T): () => T;
	<T>(fn: (v: T) => T, seed: T): () => T;
	on<T>(ev: () => any, fn: () => T): () => T;
	on<T>(ev: () => any, fn: (v: T) => T, seed: T, onchanges?: boolean): () => T;
	effect<T>(fn: () => T): void;
	effect<T>(fn: (v: T) => T, seed: T): void;

	// Data signal constructors
	data<T>(value: T): DataSignal<T>;
	value<T>(value: T, eq?: (a: T, b: T) => boolean): DataSignal<T>;

	// Batching changes
	freeze<T>(fn: () => T): T;

	// Sampling a signal
	sample<T>(fn: () => T): T;

	// Freeing external resources
	cleanup(fn: (final: boolean) => any): void;

	// experimental - methods for creating new kinds of bindings
	isFrozen(): boolean;
	isListening(): boolean;
	makeDataNode<T>(value: T): IDataNode<T>;
	makeComputationNode<T, S>(
		fn: (val: S) => T,
		seed: S,
		orphan: boolean,
		sample: true
	): { node: INode<T> | null; value: T };
	makeComputationNode<T, S>(
		fn: (val: T | S) => T,
		seed: S,
		orphan: boolean,
		sample: boolean
	): { node: INode<T> | null; value: T };
	disposeNode(node: INode<any>): void;
}

/**
 * @function
 * @name S
 * @description Global S module. Direct function invocation creates the computation dependency tree.
 * @param {() => {}} fn The computation.
 * @param {T} [value] The first value to use if the computation takes an argument (like Array.prototype.reduce())
 * @example
 * ```js
 * const a = S.data(0),
 *       b = S(() => a());
 * console.assert(b() === 0);
 * a(1);
 * console.assert(b() === 1);
 * ```
 */
const S = <S>function S<T>(fn: (v: T | undefined) => T, value?: T): () => T {
	if (SGlobals.Owner === null)
		console.warn(
			"computations created without a root or parent will never be disposed"
		);

	const { node, value: _value } = makeComputationNode(fn, value, false, false);

	const isNull = node === null;
	const returnValue = () => _value;
	const returnCurrentNode = () => node!.current();
	const computationReturn = isNull ? returnValue : returnCurrentNode;

	return function computation() {
		return computationReturn();
	};
};

// compatibility with commonjs systems that expect default export to be at require('s.js').default rather than just require('s-js')
Object.defineProperty(S, "default", { value: S });

export default S;

/**
 * @function
 * @name S.root
 * @description Creates a parent for inner computations before disposing them.
 * Avoids leaking computations to top-level without cleaning unused results.
 * All computations should be wrapped in "S.root()" unless you really need top-level.
 * @memberof S
 * @param fn Parent function to execute in the scope, like in e.g. "setInterval".
 *
 * @example
 * ```js
 * S.root(() => {
 *   const a = S.data(0);
 *   const f = S(() => a());
 *   a(1);
 * })
 * ```
 */
S.root = function root<T>(fn: (dispose: () => void) => T): T {
	const noArguments = fn.length === 0;
	const disposerFunction = function _dispose() {
		if (root === null) return;
		if (S.isFrozen()) {
			SGlobals.RootClock.disposes.add(root);
			return;
		}
		dispose(root);
	};

	const owner = SGlobals.Owner;
	const disposer = noArguments ? null : disposerFunction;
	let root = disposer === null ? UNOWNED : getCandidateNode();
	let result: T;

	SGlobals.Owner = root;

	try {
		result = disposer === null ? (fn as any)() : fn(disposer);
	} finally {
		SGlobals.Owner = owner;
	}

	if (
		disposer !== null &&
		recycleOrClaimNode(root, null as any, undefined, true)
	) {
		root = null!;
	}

	return result;
};

/**
 * @function
 * @name S.on
 * @description Wrapper around S() to excplicitely tell which nodes are dependencies.
 * @param {() => any | [() => any]} ev Function or array of functions to consider as dependencies.
 * @param {([T]) => T} fn Computation.
 * @param {T} [seed] Initial value if the computation takes an argument, like Array.prototype.reduce.
 * @param {boolean} [onchanges] If set to true, the computation won't run on definition bun only after at least one change has occured.
 * @returns {T | undefined} The return value of the computation.
 *
 * @example
 * ```js
 * const [a, b, c] = [S.data(0), S.data(0), S.data(0)];
 * const f = S.on([a, c], (count) => {
 *   a(); b(); c();
 *   return count++;
 * }, 1, true);
 * a(); // Will increment the count
 * b(); // Will not increment the count
 * c(); // Will increment the count
 * ```
 */
S.on = function on<T>(
	ev: () => any | [() => any],
	fn: (v?: T) => T,
	seed?: T,
	onchanges?: boolean
) {
	if (Array.isArray(ev)) ev = callAll(ev);
	onchanges = !!onchanges;

	return S(on, seed);

	function on(value: T | undefined) {
		const listener = SGlobals.Listener;
		ev();
		if (onchanges) onchanges = false;
		else {
			SGlobals.Listener = null;
			value = fn(value);
			SGlobals.Listener = listener;
		}
		return value;
	}
};

function callAll(ss: (() => any)[]) {
	return function all() {
		for (let i = 0; i < ss.length; i++) ss[i]();
	};
}

/**
 * @function
 * @name S.effect
 * @memberof S
 * @description Alias for S.makeComputationNode where "orphan" and "sample" are both set to false.
 * @param {([T]) => T} fn The computation.
 * @param {T} [value] The initial value of the node.
 * @example
 * ```js
 * const { node, value } = S.effect((v) => v + 1, 0);
 * console.assert(value === 0);
 * console.assert(node.current() === 0);
 * console.assert(node.next(1) === 1);
 * console.assert(node.current() === 1);
 * ```
 */
S.effect = function effect<T>(fn: (v: T | undefined) => T, value?: T): void {
	makeComputationNode(fn, value, false, false);
};

/**
 * @function
 * @name S.data
 * @memberof S
 * @description Creates a node to watch.
 * @param {T} value The initial value.
 * @returns {([T]) => T} The function that acts as a proxy to get/set the node's value.
 *
 * @example
 * ```js
 * const a = S.data(0);
 * console.assert(a() === 0);
 * console.assert(a(1) === 1);
 * console.assert(a() === 1);
 * ```
 */
S.data = function data<T>(value: T): (value?: T) => T {
	const node = new DataNode(value);

	return function data(value?: T): T {
		const noArguments = arguments.length === 0;
		return noArguments ? node.current() : node.next(value);
	};
};

/**
 * @function
 * @name S.value
 * @memberof S
 * @description Creates a node that will emit updates only when it's value becomes different (i.e., not when it is set to the current value).
 * @param {T} current The initial value of the node.
 * @param {(T,T) => boolean} [eq] A comparison function to check for equality
 * @returns {*} The function that acts as a proxy to get/set the node's value.
 *
 * @example
 * ```js
 * const a = S.value({id: 1, name: "adam"}, (x,y) => x.id === y.id);
 * const f = S.on(a, () => console.log(a()));
 * a({id: 1, name: "adam"}); // Will not fire the log.
 * a({id: 1, name: "other"}); // Will not fire the log.
 * a({id: 2, name: "other"}); // Will log the object.
 * ```
 */
S.value = function value<T>(
	current: T,
	eq?: (a: T, b: T) => boolean
): DataSignal<T> {
	const node = new DataNode(current);
	let age = -1;
	return function value(update?: T) {
		if (arguments.length === 0) {
			return node.current();
		}

		const same = eq ? eq(current, update!) : current === update;
		if (!same) {
			let time = SGlobals.RootClock.time;
			if (age === time)
				throw new Error(
					`conflicting values: "${update}" is not the same as ${current}`
				);
			[age, current] = [time, update!];
			node.next(update!);
		}
		return update!;
	};
};

/**
 * @function
 * @name S.freeze
 * @memberof S
 * @description Freeze the internal clock to apply multiple updates.
 * @param {() => T} fn The parent function of the computations.
 * @returns {T} The returned result of the computation.
 * @example
 * ```js
 * const [a, b] = [S.data(0), S.data(1)];
 * const f = S.freeze(() => {
 *   a(b()); b(b() + 1); // These changes will run together.
 *   a(); b(); // Still returns 0 and 1, changes haven't been applied yet.
 * });
 * console.assert(a() === 1 && b() === 2); // Now the changes have taken place.
 * ```
 */
S.freeze = function freeze<T>(fn: () => T): T {
	if (S.isFrozen()) {
		return fn();
	}

	let result: T = undefined!;
	SGlobals.RunningClock = SGlobals.RootClock;
	SGlobals.RunningClock.changes.reset();
	try {
		result = fn();
		event();
	} finally {
		SGlobals.RunningClock = null;
	}

	return result;
};

/**
 * @function
 * @name S.sample
 * @memberof S
 * @description Fetches a node's value without firing any event (usefull for nested computations).
 * @param {() => T} fn The data node to fetch.
 * @returns {T} The node's value.
 * @example
 * ```js
 * const a = S.data(0);
 * const f = S(() => a(S.sample(a) + 1));
 * // "a" is acted uppon in "f",
 * // but not a dependency since we don't read it, only assign a value.
 * // "S(() => a(a() + 1))" would have caused an error because "a" is a dependency,
 * // so the computation is rerun infinitely
 * ```
 */
S.sample = function sample<T>(fn: () => T): T {
	let result: T;
	const listener = SGlobals.Listener;

	SGlobals.Listener = null;
	result = fn();
	SGlobals.Listener = listener;

	return result;
};

/**
 * @function
 * @name S.cleanup
 * @memberof S
 * @description Run a given cleanup function before updating/disposing a computation.
 * @param {(boolean) => void} fn The cleanup function.
 * @example
 * ```js
 * const a = S.value(true);
 * const f = S(() => {
 *   if (a()) {
 *     const onClickHandler = e => doSomething();
 *     el.addEventListener('click', onClickHandler);
 *     S.cleanup(() => el.removeEventListener('click', onClickHandler));
 *     // Now, the event handler will be removed everytime the value of 'a' changes,
 *     // otherwise you would get multiple listeners that would never be removed.
 *   }
 * })
 * ```
 */
S.cleanup = function cleanup(fn: (final: boolean) => void): void {
	if (SGlobals.Owner === null)
		console.warn("cleanups created without a root or parent will never be run");
	else if (SGlobals.Owner.cleanups === null) SGlobals.Owner.cleanups = [fn];
	else SGlobals.Owner.cleanups.push(fn);
};

// experimental : exposing node constructors and some state
S.makeDataNode = makeDataNode;

/**
 * @function
 * @description Creates a node from a computation.
 * @param {([T]) => T} fn The computation.
 * @param {T} value The initial value of the node.
 * @param {boolean} orphan Is the computation top-level.
 * @param {boolean} sample Is the value taken by a sample (disable event listening/firing) or normally.
 * @example
 * ```js
 * const { node, value } = S.makeComputationNode((v) => v + 1, 0, false, false);
 * console.assert(value === 0);
 * console.assert(node.current() === 0);
 * console.assert(node.next(1) === 1);
 * console.assert(node.current() === 1);
 * ```
 */
S.makeComputationNode = makeComputationNode;

/**
 * @function
 * @name S.disposeNode
 * @memberof S
 * @description Disposes of the node, cleanup function.
 * @param {ComputationNode} node The node to clean.
 * @example
 * ```js
 * const { node, value } = S.effect((v) => v + 1, 0);
 * S.disposeNode(node);
 * ```
 */
S.disposeNode = disposeNode;

/**
 * @function
 * @name S.isFrozen
 * @memberof S
 * @description Checks if the internal clock of S is frozen (i.e. called from S.freeze()).
 * @returns {boolean} True if frozen.
 * @example
 * ```js
 * // From top-level...
 * console.assert(S.isFrozen() === false);
 * S.freeze(() => {
 *   console.assert(S.isFrozen() === true);
 * });
 * console.assert(S.isFrozen() === false);
 * ```
 */
S.isFrozen = isFrozen;

/**
 * @function
 * @name S.isListening
 * @memberof S
 * @description Checks if the listener is on (disabled in S.sample()).
 * @returns {boolean} True if listening.
 * @example
 * ```js
 * console.assert(S.isListening() === true)
 * ```
 */
S.isListening = isListening;
