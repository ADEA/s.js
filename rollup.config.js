export default {
	input: "dist/es/S.js",
	output: [
		{
			file: "dist/S.mjs",
			format: "es",
		},
		{
			file: "dist/S.cjs",
			format: "cjs",
		},
		{
			file: "dist/S.js",
			format: "umd",
			name: "S",
		},
	],
};
